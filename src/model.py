import torch
import torch.nn as nn
import torchvision.models as models
from numpy.linalg import norm
from weldon import WeldonPool2d

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Model(nn.Module):
    def __init__(self, batch_size):
        super(Model, self).__init__()
      
        self.batch_size = batch_size
        resnet =  models.resnet152().cuda()
        self.base_layer = nn.Sequential(*list(resnet.children())[:-2]).cuda()
        #for params in self.base_layer.parameters():
        #     params.requires_grad = False
        self.spaConv = nn.Conv2d(2048, 2400, 1).cuda()
        #for params in self.spaConv.parameters():
        #    params.requires_grad = False
        self.fc = nn.Linear(2400,2400).cuda()
        #for params in self.image_affine.parameters():
        #    params.requires_grad = False
        
        self.sru = nn.GRU(300,2400,num_layers=4).cuda()
        #for params in self.sru.parameters():
        #    params.requires_grad = False


    def forward(self, imagetensors, wordvectorspositive, wordvectorsnegative):
        #image stream
        output = self.base_layer(imagetensors)
        output = self.spaConv(output)
        pool = WeldonPool2d(15,None).cuda()
        output = pool.forward(output)
        output = self.fc(output)
        output = output.view(self.batch_size,-1)
        imagesoutput = []#output.cuda()#.detach()
        count = 0
        for image in output.cuda():
            l2norm = torch.norm(image)
            _l2 = torch.empty(2400,dtype=torch.float)
            _l2.fill_(l2norm.item())
            imagesoutput.append(image / _l2.cuda())
            count += 1

        # text stream
        normw2vposvecs = []
        normw2vnegvecs = []
        for wordvectorpositive in wordvectorspositive:
            srupositiveoutput = self.sru(wordvectorpositive.unsqueeze_(0).cuda()) 
            srupositiveoutput = srupositiveoutput[0][0][-1]#.detach()
            l2norm = torch.norm(srupositiveoutput)
            _l2 = torch.empty(2400,dtype=torch.float)
            _l2.fill_(l2norm.item())
            normw2vpos = srupositiveoutput
            normw2vposvecs.append(normw2vpos / _l2.cuda())
            #print(normw2vpos) 
        for wordvectornegative in wordvectorsnegative:
            srunegativeoutput = self.sru(wordvectornegative.unsqueeze_(0).cuda())
            srunegativeoutput = srunegativeoutput[0][0][-1]#.detach()
            l2norm = torch.norm(srunegativeoutput)
            _l2 = torch.empty(2400,dtype=torch.float)
            _l2.fill_(l2norm.item())
            normw2vneg = srunegativeoutput
            normw2vnegvecs.append(normw2vneg / _l2.cuda())

        imagesoutput = torch.stack(imagesoutput) 
        normw2vposvecs = torch.stack(normw2vposvecs)
        normw2vnegvecs = torch.stack(normw2vnegvecs)
        return (imagesoutput, normw2vposvecs, normw2vnegvecs)
