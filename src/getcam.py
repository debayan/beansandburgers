import torch,sys,gensim,json,os
from evalmodel import Model
from torchvision import transforms
from PIL import Image, ImageDraw
from torch.autograd import Variable
from shutil import copyfile
from numpy import unravel_index
import cv2
import numpy as np
from scipy.misc import imread, imsave

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
w2vmodel = gensim.models.KeyedVectors.load_word2vec_format('/data/debayan/beansandburgers/data/GoogleNews-vectors-negative300.bin', binary=True)

batch_size = 1
m = Model(batch_size).cuda()
loadmodel = m.load_state_dict(torch.load(sys.argv[1]))

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])
imsize = 256
loader = transforms.Compose([transforms.Scale(imsize), transforms.CenterCrop(224), transforms.ToTensor(), normalize])

cocolabels = ['person','bicycle','car','motorcycle','airplane','bus','train','truck','boat','traffic light','fire hydrant','stop sign','parking meter','bench','bird','cat','dog','horse','sheep','cow','elephant','bear','zebra','giraffe','backpack','umbrella','handbag','tie','suitcase','frisbee','skis','snowboard','sports ball','kite','baseball bat','baseball glove','skateboard','surfboard','tennis racket','bottle','wine glass','cup','fork','knife','spoon','bowl','banana','apple','sandwich','orange','broccoli','carrot','hot dog','pizza','donut','cake','chair','couch','potted plant','bed','dining table','toilet','tv','laptop','mouse','remote','keyboard','cell phone','microwave','oven','toaster','sink','refrigerator','book','clock','vase','scissors','teddy bear','hair drier','toothbrush']

def image_loader(image_name):
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = loader(image).float()
    image = Variable(image)#, requires_grad=True)
    return image.cuda()  #assumes that you're using GPU
    #return image

def chunks(l, n):
    n = max(1, n)
    return (l[i:i+n] for i in range(0, len(l), n))

annotdict = {}

imgvecdict = {}
txtvecdict = {}

f = open('../data/val2014.txt') 
for line in f.readlines():
    try:
        id = line.strip()
        imagefile = '/data/debayan/beansandburgers/data/val2014/COCO_val2014_'+str(id)+'.jpg'
        imageoutputs = None
        w2vs = None
        image = image_loader(imagefile)
        with torch.no_grad():
            try:
                imagestensors = torch.stack([image])
            except:
                print("error")
                continue
        (imagesoutputs, _) = m(imagestensors.cuda(), -1, True, False)
        for label in cocolabels:
            if label not in txtvecdict:
                wordvector = []
                for word in label.split(' '):
                    wordvector.append(w2vmodel[word])
                (_, w2vs) = m(-1, [torch.FloatTensor(wordvector)], False, True)
                txtvecdict[label] = w2vs
            else:
                w2vs = txtvecdict[label]
        
            for w2v,imageoutput in zip(w2vs,imagesoutputs):
                vals,indices = torch.topk(w2v,180)
                heatmap = torch.zeros(7, 7, dtype=torch.float).cuda()
                for val,index in zip(vals,indices):
                    heatmap += val  *  imageoutput[index]
                heatmap = heatmap.detach()
                heatmap = heatmap.cpu().numpy()
                heatmap = cv2.resize(heatmap, dsize=(256,256), interpolation=cv2.INTER_LANCZOS4)
                heatmap = (heatmap - heatmap.min()) / (heatmap.max() - heatmap.min())
                try:
                    os.stat("../data/segout/"+label)
                except:
                    os.mkdir("../data/segout/"+label)
                imsave("../data/segout/"+label+"/COCO_val2014_"+str(id)+"_conf.png", heatmap)
                heatmap_binary = heatmap.copy()
                threshold = 0.5*(heatmap.max() - heatmap.min())
                heatmap_binary[heatmap_binary < threshold] = 0
                heatmap_binary[heatmap_binary >= threshold] = 255
                
                imsave( "../data/segout/"+label+"/COCO_val2014_"+str(id)+"_binary.png", heatmap_binary)
                oimg = cv2.imread(imagefile)
                rimg = cv2.resize(oimg,(256,256))
                cv2.imwrite("../data/segout/"+label+"/COCO_val2014_"+str(id)+".jpg", rimg)
    except:
        print("error")
        continue

