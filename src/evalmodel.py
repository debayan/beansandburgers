import torch,sys
import torch.nn as nn
import torchvision.models as models
from numpy.linalg import norm
from weldon import WeldonPool2d
from sru import SRU, SRUCell

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Model(nn.Module):
    def __init__(self, batch_size):
        super(Model, self).__init__()
      
        self.batch_size = batch_size
        self.resnet =  models.resnet152(pretrained=True).cuda()
        resnetconv5 = self.resnet._modules.get('layer4')
        h = resnetconv5.register_forward_hook(self.copy_data)
        self.conv = nn.Conv2d(2048,2400,1)

        self.image_affine = nn.Linear(2400,2400)
        self.sru = SRU(300,2400,num_layers=4).cuda()

    def copy_data(self, m, i, o):
        self.resnetconv5output = torch.zeros(o.data.shape)
        self.resnetconv5output.copy_(o.data)

    def forward(self, imagetensors, wordvectors, hasimage=True, hastxt=True):
        #image stream
        if hasimage:
            output = self.resnet(imagetensors)
            imagesoutput = self.conv(self.resnetconv5output.cuda())
            for i in range(self.batch_size):
                for r in range(7):
                    for c in range(7):
                        imagesoutput[i,:,r,c] =  self.image_affine(imagesoutput[i,:,r,c])
            return (imagesoutput, None)

        # text stream
        if hastxt:
            normw2vvecs = []
            for wordvector in wordvectors:
                sruoutput = self.sru(wordvector.unsqueeze_(0).cuda()) 
                sruoutput = sruoutput[0][0][-1]#.detach()
                l2norm = torch.norm(sruoutput)
                _l2 = torch.empty(2400,dtype=torch.float)
                _l2.fill_(l2norm.item())
                normw2v = sruoutput
                normw2vvecs.append(normw2v / _l2.cuda())
            normw2vvecs = torch.stack(normw2vvecs)
            return (None, normw2vvecs)
            #print(normw2vpos) 
