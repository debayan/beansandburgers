import torch
from torch import optim
import torch.nn as nn
import torchvision.models as models
import numpy as np
from random import randint
from torchvision import transforms
import torchvision.models as models
from torch.autograd import Variable
import sys,os,json,random,gensim
from model import Model
from PIL import Image
from siamesetriplet.losses import TripletLoss

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
w2vmodel = gensim.models.KeyedVectors.load_word2vec_format('../data/GoogleNews-vectors-negative300.bin', binary=True)

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])
imsize = 256
loader = transforms.Compose([transforms.Scale(imsize), transforms.CenterCrop(224), transforms.ToTensor(), normalize])

def image_loader(image_name):
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = loader(image).float()
    image = Variable(image)#, requires_grad=True)
    return image.cuda()  #assumes that you're using GPU

def chunks(l, n):
    n = max(1, n)
    return (l[i:i+n] for i in range(0, len(l), n))

idscaptions = []
f = open('../data/traincoco.txt') #Training
for line in f.readlines():
    line = line.strip()
    d = json.loads(line)
    idscaptions.append((d['image_id'],d['captions']))
f.close()

batch_size = 15
m = Model(batch_size)#batch size
print(sys.argv)
#model_optimizer = optim.Adam(filter(lambda p: p.requires_grad,m.parameters()), lr=0.01)
#model_optimizer = optim.Adam(filter(lambda p: p.requires_grad,m.parameters()))
model_optimizer  = optim.Adam(filter(lambda p: p.requires_grad,m.parameters()), lr=0.0001)
if len(sys.argv) > 1:
    if sys.argv[1]:
        print("Loading previously saved model: %s"%sys.argv[1])
        checkpoint = torch.load(sys.argv[1])
        start_epoch = checkpoint['epoch']
        m.load_state_dict(checkpoint['state_dict'])
        #m.fc = nn.Linear(2400,2400).cuda()
        #m.sru = SRU(300,2400,num_layers=4).cuda()
criterion = TripletLoss(margin=1.0)
epoch = 1
mini_batch_loss = 0
while epoch <= 50: #epoch
    trainidbatches = chunks(idscaptions,batch_size)
    batchcount = 0
    for trainidbatch in trainidbatches:
        if len(trainidbatch) < batch_size:
            continue
        trainingtuples = []
        for trainid in trainidbatch:
            imagefile = '../data/train2014/COCO_train2014_'+str(trainid[0])+'.jpg'
            image = image_loader(imagefile)
            w2v = []
            for word in trainid[1][0].split(' '):
                try:
                    w2v.append(w2vmodel[word])
                except:
                    continue
            trainingtuples.append((image,w2v))
        images = []
        wordvectorstrue = []
        wordvectorsfalse = []
        for tup in trainingtuples:
            images.append(tup[0])
            wordvectorstrue.append(torch.FloatTensor(tup[1]))
            wordvectorsfalse.append(torch.FloatTensor(trainingtuples[random.randint(0,batch_size-1)][1]))
        try:
            imagestensors = torch.stack(images)
        except:
            print("error")
            continue
        (imagesoutput, w2vpos, w2vneg) = m(imagestensors.cuda(), wordvectorstrue, wordvectorsfalse)
        #print(imagesoutput.shape,w2vpos.shape, w2vneg.shape)
        #print(torch.nn.functional.cosine_similarity(imagesoutput.cuda(), w2vpos,dim=1))
        #print(torch.nn.functional.cosine_similarity(imagesoutput.cuda(), w2vneg,dim=1))
        #print(torch.nn.functional.cosine_similarity(w2vpos, w2vneg,dim=1))
        loss = criterion(imagesoutput.cuda(),w2vpos, w2vneg)
        loss.backward()
        mini_batch_loss += loss.item()
        if batchcount > 0 and batchcount%11 == 0:
            model_optimizer.step()
            model_optimizer.zero_grad()
            print("epoch = %d batch = %d batchmax = %d loss = %f"%(epoch,batchcount+1,len(idscaptions)/batch_size,mini_batch_loss/11.0))
            mini_batch_loss = 0
        if batchcount > 0 and batchcount%50 == 0:
            state = {'epoch': epoch, 'state_dict': m.state_dict(),
             'optimizer': model_optimizer.state_dict()}
            torch.save(state, 'model_weights.pytorch')
        batchcount += 1
    epoch += 1    
