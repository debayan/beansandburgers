import sys,os,json,cv2
import numpy as np
import matplotlib.pyplot as plt 


cocolabels = ['person','bicycle','car','motorcycle','airplane','bus','train','truck','boat','traffic light','fire hydrant','stop sign','parking meter','bench','bird','cat','dog','horse','sheep','cow','elephant','bear','zebra','giraffe','backpack','umbrella','handbag','tie','suitcase','frisbee','skis','snowboard','sports ball','kite','baseball bat','baseball glove','skateboard','surfboard','tennis racket','bottle','wine glass','cup','fork','knife','spoon','bowl','banana','apple','sandwich','orange','broccoli','carrot','hot dog','pizza','donut','cake','chair','couch','potted plant','bed','dining table','toilet','tv','laptop','mouse','remote','keyboard','cell phone','microwave','oven','toaster','sink','refrigerator','book','clock','vase','scissors','teddy bear','hair drier','toothbrush']


valids = []
f = open('../data/val2014.txt')
for id in f.readlines():
    id = id.strip()
    valids.append(id)
f.close()

labelcount = 1
ious = []
averageprecisions = []
for label in cocolabels:
    print("Label = %s %d"%(label, labelcount))
    tp = 0
    fp = 0
    fn = 0
    totalcount = 0
    classcounter = 0
    tpconfidences = []
    fpconfidences = []
    fnconfidences = []
    records = []
    classious = []
    for id in valids:
        valconfimgpath = '../data/segout/'+label+'/COCO_val2014_'+id+'_conf.png'
        valmaskimgpath = '../data/segout/'+label+'/COCO_val2014_'+id+'_binary.png'
        valgtimgpath = '../data/ClassSegmentation/val2014/COCO_val2014_'+id+'.png'
        valconfimg = cv2.imread(valconfimgpath, cv2.IMREAD_GRAYSCALE)
        valmaskimg =  cv2.imread(valmaskimgpath, cv2.IMREAD_GRAYSCALE)
        valgtimg = cv2.imread(valgtimgpath, cv2.IMREAD_GRAYSCALE)
        if valconfimg is None or valmaskimg is None or valgtimg is None:
            continue
        totalcount += 1
        if labelcount not in valgtimg:
            continue
        valgtimg = cv2.resize(valgtimg, (256,256))
        valgtimg[valgtimg != labelcount] = 0
        valgtimg[valgtimg == labelcount] = 1

        valmaskimg[valmaskimg == 255] = 1
        valconfimg = (valconfimg - valconfimg.min())/(valconfimg.max() - valconfimg.min())
        maskconfimg = np.multiply(valmaskimg,valconfimg)
        conf = np.sum(maskconfimg)/np.count_nonzero(maskconfimg)
        overlap = np.multiply(valgtimg,valmaskimg)
        union = valgtimg + valmaskimg
        IOU = overlap.sum()/float(union.sum())
        records.append((id,conf,IOU))
        classious.append(IOU)
        ious.append(IOU)
        try:
            os.stat("../data/s/"+label)
        except:
            os.mkdir("../data/s/"+label)
        cv2.imwrite('../data/s/'+label+'/COCO_val2014_'+id+'_conf.png',valconfimg*255)
        cv2.imwrite('../data/s/'+label+'/COCO_val2014_'+id+'_gt.png',valgtimg*255)
        cv2.imwrite('../data/s/'+label+'/COCO_val2014_'+id+'_mask.png',valmaskimg*255)
        cv2.imwrite('../data/s/'+label+'/COCO_val2014_'+id+'_overlap.png',overlap*255)
    x = []
    y = []
    prs = []
    for th in np.arange(1.0,0.0,-0.01):
        truepositives = []
        falsepositives = []
        falsenegatives = []
        for record in records:
            if record[1] > th:
                if record[2] >= 0.3:
                    truepositives.append(record)
                elif record[2] > 0.0 and record[2] < 0.3:
                    falsepositives.append(record)
                    falsenegatives.append(record)
                else:
                    falsenegatives.append(record)
        if (len(truepositives) + len(falsepositives)) == 0 or (len(truepositives) + len(falsenegatives)) == 0:
            x.append(0)
            y.append(1)
            continue
        precision = len(truepositives)/float(len(truepositives) + len(falsepositives))
        recall = len(truepositives)/float(len(truepositives) + len(falsenegatives))
        x.append(recall)
        y.append(precision)
        prs.append((recall,precision))
    # From section 4.2 of http://homepages.inf.ed.ac.uk/ckiw/postscript/ijcv_voc09.pdf to achieve a smooth PR curve
    newprecisions = []
    for x in np.arange(0.0,1.1,0.1):
        maxprecision = 0
        for pr in prs:
            if pr[0] >= x:
                if pr[1] >= maxprecision:
                    maxprecision = pr[1]
        newprecisions.append(maxprecision)
    newrecalls = list(np.arange(0.0,1.1,0.1))
    averageprecision = sum(newprecisions)/11.0
    averageprecisions.append(averageprecision)
    plt.plot(newrecalls, newprecisions) 
    plt.savefig('plots/plt%s.png'%labelcount)  
    plt.clf()
    labelcount += 1
    print("class average precision = %f"%averageprecision)
    print("mean average precision = %f"%(sum(averageprecisions)/len(averageprecisions)))
    print("class IOU = %f"%(sum(classious)/float(len(classious))))
    print("total IOU = %f"%(sum(ious)/float(len(ious))))
